// Copyright © 2018 MATHEW, Charlie. All rights reserved.

import Foundation

extension Decodable {

    static func decode(fromJSONData jsonData: Data) throws -> Self {
        let decoder = JSONDecoder()
        return try decoder.decode(self, from: jsonData)
    }
}

