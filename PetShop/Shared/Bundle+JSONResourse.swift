// Copyright © 2018 MATHEW, Charlie. All rights reserved.

extension Bundle {

    func jsonData(forResource resource: String) -> Data {
        let resourceURL: URL = url(forResource: resource, withExtension: "json")!
        let data: Data = try! Data(contentsOf: resourceURL)

        return data
    }
}

import Foundation
