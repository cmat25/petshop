// Copyright © 2018 MATHEW, Charlie. All rights reserved.

enum RepositoryError: Error {

    /// The last request timed out.
    case timedOut

    /// General error.
    case general
}
