// Copyright © 2018 MATHEW, Charlie. All rights reserved.

enum APIError: Error {

    /// A call was made for data, but none was returned in the response body.
    case dataNotFound

    /// Error encountered during conversion
    case conversionError

    /// Something completetly unexpected occured.
    case unexpected(Error)
}

extension APIError {

    /// Maps a server API error to a Domain layer error.
    var asDomainError: Error {

        switch self {
        case .dataNotFound:
            return RepositoryError.timedOut

        case .conversionError:
            return RepositoryError.timedOut

        default:
            return RepositoryError.general
        }
    }
}
