// Copyright © 2018 MATHEW, Charlie. All rights reserved.

protocol PetListViewModelType {

    var screenTitle: String { get }

    var hasError: Bool { get }

    func loadPets(callback: @escaping () -> ())

    func numberOfSection() -> Int

    func numberOfRows(at section: Int) -> Int

    func sectionTitle(at section: Int) -> String

    func getName(at indexPath: IndexPath) -> String
}

final class PetListViewModel: PetListViewModelType {

    // MARK: - Lifecycle

    init(respository: PetDetailsListRepository) {
        self.respository = respository
    }

    // MARK: - Conformance

    // MARK: PetListViewModelType

    let screenTitle: String = "Cats"

    var hasError: Bool {
        return (error != nil) ? true : false
    }

    func loadPets(callback: @escaping () -> ()) {
        self.callback = callback
        cats
            .subscribe(
                onSuccess: { self.dictionary = $0 },
                onError: { self.error = $0 }
            ).disposed(by: disposeBag)
    }

    func numberOfSection() -> Int {
        return dictionary.count
    }

    func numberOfRows(at section: Int) -> Int {
        return Array(dictionary.values)[section].count
    }

    func sectionTitle(at section: Int) -> String {
        return Array(dictionary.keys)[section]
    }

    func getName(at indexPath: IndexPath) -> String {
        let value: [Pet] = Array(dictionary.values)[indexPath.section]
        return value[indexPath.row].name ?? ""
    }

    // MARK: - Private

    // MARK: - Property

    private let respository: PetDetailsListRepository!

    private var callback: () -> () = {}

    private var cats: Single<[String : [Pet]]> {
        return respository
            .getDetailsList()
            .map { $0.filter { $0.type == "Cat" } }
            .map(mapToDictionary(from:))
    }

    private var dictionary: [String : [Pet]] = [:] {
        didSet {
            callback()
        }
    }

    private var error: Error! {
        didSet {
            callback()
        }
    }

    private let disposeBag: DisposeBag = .init()

    // MARK: Function

    private func mapToDictionary(from pets: [Pet]) -> [String : [Pet]] {
        let predicate = { (element: Pet) in
            return element.gender ?? ""
        }
        return Dictionary(grouping: pets, by: predicate)
    }

}

import RxSwift
