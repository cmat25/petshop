// Copyright © 2018 MATHEW, Charlie. All rights reserved.

final class PetListViewController: UIViewController {

    // MARK: - Outlets

    @IBOutlet private var tableView: UITableView! {
        didSet {
            tableView.tableFooterView = UIView()
            tableView.dataSource = self
        }
    }

    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        setInterface()
    }

    // MARK: - Dependency Injection

    var viewModel: PetListViewModelType!

    // MARK: - Private

    // MARK - Properties

    private var disposeBag: DisposeBag = DisposeBag()

    // MARK: Function

    private func setInterface() {
        title = viewModel.screenTitle

        viewModel.loadPets(callback: {[unowned self] in
            self.viewModel.hasError ? self.showError() : self.tableView.reloadData()
        })
    }

    private func showError() {
        let alert = UIAlertController(title: "Error", message: "No data found", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "done", style: .cancel, handler: nil))
        self.present(alert, animated: true)
    }
}

// MARK: - Extension

// MARK - UITableViewDataSource

extension PetListViewController: UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.numberOfSection()
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRows(at: section)
    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return viewModel.sectionTitle(at: section)
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell = UITableViewCell()
        cell.textLabel?.text = viewModel.getName(at: indexPath)
        return cell
    }
}

import UIKit
import RxSwift
