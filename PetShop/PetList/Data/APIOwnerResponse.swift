// Copyright © 2018 MATHEW, Charlie. All rights reserved.

struct APIOwnerResponse: Decodable {

    let name: String?
    let gender: String?
    let age: Int?
    let pets: [Pet]?

    struct Pet: Decodable {
        let name: String?
        let type: String?
    }
}
