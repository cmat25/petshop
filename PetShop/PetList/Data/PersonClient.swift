// Copyright © 2018 MATHEW, Charlie. All rights reserved.

protocol PersonClientType {
    func getPetOwners() -> Single<[APIOwnerResponse]>
}

final class PersonClient: PersonClientType {

    // MARK: - Lifecycle

    init(client: HTTPClientType) {
        self.client = client
    }

    // MARK: - Conformance

    // MARK: PersonClientType

    func getPetOwners() -> Single<[APIOwnerResponse]> {
        return client.singleDataTask(with: urlString)
    }

    // MARK: - Private

    // MARK: Properties

    private let client: HTTPClientType

    private let urlString = "people.json"
}

import RxSwift
