// Copyright © 2018 MATHEW, Charlie. All rights reserved.

final class PetDetailsListService: PetDetailsListRepository {

    // MARK: - Lifecycle

    init(client: PersonClientType, mapper: APIPetMapping) {
        self.client = client
        self.mapper = mapper
    }

    // MARK: - Conformance

    // MARK: PetDetailsListRepository

    func getDetailsList() -> Single<[Pet]> {
        return client
            .getPetOwners()
            .map(mapper.mapToPets)
            .mapErrorsToDomain()
    }

    // MARK: - Private

    // MARK: - Properties

    private var client: PersonClientType!

    private var mapper: APIPetMapping!
}

// MARK: - Observables

public extension PrimitiveSequence {

    public func mapErrorsToDomain() -> PrimitiveSequence<Trait, Element> {
        return catchError { error -> PrimitiveSequence<Trait, Element> in
            if let apiError = error as? APIError {
                throw apiError.asDomainError
            }
            throw error
        }
    }
}

import RxSwift
