// Copyright © 2018 MATHEW, Charlie. All rights reserved.

protocol APIPetMapping {
    func mapToPets(from ownerResponse: [APIOwnerResponse]) -> [Pet]
}

final class APIPetMapper: APIPetMapping {

    func mapToPets(from ownerResponse: [APIOwnerResponse]) -> [Pet] {
        return ownerResponse
            .map { ($0.gender, $0.pets ) }
            .map(mapToPets)
            .flatMap { $0 }
    }

    func mapToPets(gender: String?, pets: [APIOwnerResponse.Pet]?) -> [Pet] {
        return pets
            .flatMap { $0
                .map { Pet.init(name: $0.name, type: $0.type, gender: gender) }
            } ?? []
    }
}
