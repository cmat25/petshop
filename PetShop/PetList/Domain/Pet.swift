// Copyright © 2018 MATHEW, Charlie. All rights reserved.

struct Pet {

    let name: String?
    let type: String?
    let gender: String?
}
