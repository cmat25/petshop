// Copyright © 2018 MATHEW, Charlie. All rights reserved.

protocol PetDetailsListRepository {

    func getDetailsList() -> Single<[Pet]>
}

import RxSwift
