// Copyright © 2018 MATHEW, Charlie. All rights reserved.

protocol HTTPClientType {

    func singleDataTask<T>(with urlPath: String) -> Single<T> where T: Decodable
}

final class HTTPClient: HTTPClientType {

    // MARK: - Initialiser

    init(baseURL: String, configuration: URLSessionConfiguration = .default) {
        self.baseURL = baseURL
        self.session = URLSession(configuration: configuration, delegate: nil, delegateQueue: OperationQueue.main)
    }

    // MARK: - Conformance

    // MARK: HTTPClientType

    func singleDataTask<T>(with urlPath: String) -> Single<T> where T: Decodable {
        return Single<T>.create { [unowned self] single in

            let urlString: String = self.baseURL+urlPath
            let url: URL! = URL(string: urlString)
            var urlRequest = URLRequest(url: url)
            urlRequest.httpMethod = "GET"

            let task = self.session.dataTask(with: urlRequest) { data, response, error in

                if let error = error {
                    single(.error(APIError.unexpected(error)))
                    return
                }

                if let data = data, !data.isEmpty {
                    self.decodeResponse(data: data, single: single)
                } else {
                    single(.error(APIError.dataNotFound))
                }
            }

            task.resume()

            return Disposables.create {
                task.cancel()
            }
        }
    }

    // MARK: - Private Properties

    private let baseURL: String!

    private let session: URLSession

    // MARK: - Private Function

    private func decodeResponse<T>(data: Data, single: @escaping (SingleEvent<T>) -> Void) where T: Decodable {
        do {
            let values = try T.decode(fromJSONData: data)
            single(.success(values))
        } catch {
            single(.error(APIError.conversionError))
        }
    }
}

import Foundation
import RxSwift

