//
//  AppDelegate.swift
//  PetShop
//
//  Created by MATHEW, Charlie on 15/3/18.
//  Copyright © 2018 MATHEW, Charlie. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var appStartupWindow: UIWindow? = UIWindow()


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {

        let baseURL: String = "http://agl-developer-test.azurewebsites.net/"
        let httpClient: HTTPClientType = HTTPClient(baseURL: baseURL)
        let personClient: PersonClientType = PersonClient(client: httpClient)
        let repository: PetDetailsListRepository = PetDetailsListService(
            client: personClient,
            mapper: APIPetMapper()
        )

        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let initialViewController: PetListViewController = storyboard.instantiateViewController(withIdentifier: "PetListViewController") as! PetListViewController
        initialViewController.viewModel = PetListViewModel(respository: repository)
        let navigationController = UINavigationController(rootViewController: initialViewController)

        self.window?.rootViewController = navigationController
        self.window?.makeKeyAndVisible()

        return true
    }
}

