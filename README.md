# PetShop

## Synopsis

### Xcode Support

Xcode version - 9.2

### Project Setup

This repository contains a simple sample app using the AGL's API to display a list of cats, to be used as a coding assignment for AGL's developer candidate.

## Installation/Building

To install the dependencies
* Open a terminal and cd to the directory containing the Podfile
* Run the `pod install` command

(For further details regarding cocoapod installation see https://cocoapods.org/)

## Architecture

The PetShop app is structured following the Clean Architecture architectural pattern.
The UI, presentation logic, business logic and data management logic are clearly separated in layers.

## Testing

### Unit Testing

The majority of the unit tests are implemented in all the layer, specifically targeting application business logic that is completely independent from iOS framework dependencies.
Tests in this area are thus deemed to be of high value in preventing regressions.
