// Copyright © 2018 MATHEW, Charlie. All rights reserved.

final class Bundle_JSONResourseSpec: QuickSpec {

    override func spec() {

        describe("a method to access a resource with .json extension") {

            var jsonData: Data!

            context("when resource is present") {

                jsonData = Bundle(for: type(of: self)).jsonData(forResource: "MockSampleJSON")

                it("returns valid data") {
                    expect(jsonData).notTo(beNil())
                }
            }

            context("when resource is not present") {

                it("throws fatal error") {
                    let crash: () -> Void = {
                        jsonData = Bundle(for: type(of: self)).jsonData(forResource: "NoName")
                    }
                    expect(crash()).to(throwAssertion())
                }
            }
        }
    }
}

@testable import PetShop
import Quick
import Nimble

