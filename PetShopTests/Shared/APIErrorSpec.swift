// Copyright © 2018 MATHEW, Charlie. All rights reserved.

final class APIErrorSpec: QuickSpec {

    override func spec() {

        describe("APIError tests") {
            context("asDomainError mapping") {

                it("Maps timeout") {
                    expect(APIError.dataNotFound.asDomainError).to(matchError(RepositoryError.timedOut))
                }

                it("Maps timeout") {
                    expect(APIError.conversionError.asDomainError).to(matchError(RepositoryError.timedOut))
                }

                it("Maps authenticationFailed") {
                    expect(APIError.unexpected(APIError.dataNotFound).asDomainError).to(matchError(RepositoryError.general))
                }
            }
        }
    }
}

@testable import PetShop
import Nimble
import Quick

