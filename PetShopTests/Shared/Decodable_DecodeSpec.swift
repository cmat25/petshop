// Copyright © 2018 MATHEW, Charlie. All rights reserved.

final class Decodable_DecodeSpec: QuickSpec {

    override func spec() {

        describe("a Decodable object") {

            var model: TestModel!
            var jsonData: Data!

            beforeEach {
                jsonData = """
                {
                    "attribute": "value"
                }
                """.data(using: .utf8)! as Data
            }

            it("") {
                expect { model = try TestModel.decode(fromJSONData: jsonData) }.toNot(throwError())

                expect(model).toNot(beNil())
                expect(model?.attribute) == "value"
            }

        }

        describe("a Decodable object") {

            var model: TestModel!
            var jsonData: Data!

            beforeEach {
                jsonData = Bundle(for: type(of: self)).jsonData(forResource: "MockSampleJSON")
            }

            it("") {
                expect { model = try TestModel.decode(fromJSONData: jsonData) }.toNot(throwError())
                expect(model).toNot(beNil())
                expect(model?.attribute) == "apple"
            }

        }
    }
}

private struct TestModel: Decodable {

    let attribute: String
}

@testable import PetShop
import Nimble
import Quick

