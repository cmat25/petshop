// Copyright © 2018 MATHEW, Charlie. All rights reserved.

final class HTTPClientSpec: QuickSpec {

    override func spec() {

        describe("a HTTPClient") {

            var client: HTTPClientType!

            beforeEach {
                client = HTTPClient(baseURL: "")
            }

            afterEach {
                client = nil
            }

            it("initialises") {
                expect(client).toNot(beNil())
            }

            describe("request made") {

                var disposable: Disposable!
                var value: Data?
                var error: Error?

                afterEach {
                    value = nil
                    error = nil
                    disposable.dispose()
                    disposable = nil
                }

                context("when URL is valid") {

                    let url: URL! = Bundle(for: type(of: self)).url(forResource: "MockSampleJSON", withExtension: "json")

                    it("gives a valid response") {
                        disposable = client.singleDataTask(with: url.absoluteString).subscribe(onSuccess: { value = $0 })
                        expect(value).toEventually(beNil())
                    }

                    it("does give errors") {
                        disposable = client
                            .singleDataTask(with: url.absoluteString)
                            .subscribe(onSuccess: { value = $0 }, onError: { error = $0 })

                        expect(error).toEventuallyNot(beNil())
                    }
                }

                context("when URL is invalid") {
                    let urlPath: String = "sample.com"

                    it("doesnot return a response") {
                        disposable = client.singleDataTask(with: urlPath).subscribe(onSuccess: { value = $0 })
                        expect(value).toEventually(beNil())
                    }

                    it("returns an error") {
                        disposable = client
                            .singleDataTask(with: urlPath)
                            .subscribe(onSuccess: { value = $0 }, onError: { error = $0 })

                        expect(value).toEventually(beNil())
                        expect(error).toEventuallyNot(beNil())
                        expect(error).toEventually(beAnInstanceOf(APIError.self))
                    }
                }
            }
        }
    }
}

@testable import PetShop
import Nimble
import Quick
import RxSwift

