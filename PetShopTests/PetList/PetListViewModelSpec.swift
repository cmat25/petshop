// Copyright © 2018 MATHEW, Charlie. All rights reserved.

final class PetListViewModelSpec: QuickSpec {

    override func spec() {

        describe("a PetListViewModel") {

            var model: PetListViewModelType!
            var repository: PetDetailsListRepository!

            beforeEach {
                repository = MockService()
                model = PetListViewModel(respository: repository)
                model.loadPets(callback: {})
            }

            afterEach {
                repository = nil
                model = nil
            }

            it("initialises") {
                expect(model).toNot(beNil())
            }

            describe("loads the list of pets") {

                var reloadTable: Bool?

                beforeEach {
                    model.loadPets(callback: { reloadTable = true })
                }

                it("contains screen title") {
                    expect(model.screenTitle) == "Cats"
                }

                it("gives number of section in the table") {
                    let number: Int = model.numberOfSection()
                    expect(number) == 2
                }

                it("reloads the table") {
                    expect(reloadTable) == true
                }
            }

            describe("configure table view section") {

                context("when section is valid") {

                    it("returns a valid title for all sections") {
                        expect(model.sectionTitle(at: 0)) == "Male"
                        expect(model.sectionTitle(at: 1)) == "Female"
                    }

                    it("returns number of rows for all sections") {
                        expect(model.numberOfRows(at: 0)) == 1
                        expect(model.numberOfRows(at: 1)) == 1
                    }
                }

                context("when section is invalid") {

                    it("throws fatal error") {
                        let crash: () -> Void = {
                            _ = model.sectionTitle(at: 3)
                        }
                        expect(crash()).to(throwAssertion())
                    }
                }
            }

            describe("fetch pet name") {

                context("given a valid index path") {

                    it("gives the name of the pet") {
                        expect(model.getName(at: IndexPath.init(row: 0, section: 0))) == "tom"
                        expect(model.getName(at: IndexPath.init(row: 0, section: 1))) == "tina"
                    }
                }

                context("when section is invalid") {

                    it("throws fatal error") {
                        let crash: () -> Void = {
                            _ = model.getName(at: IndexPath.init(row: 1, section: 0))
                        }
                        expect(crash()).to(throwAssertion())
                    }
                }
            }

            describe("network times out") {

                var displayAlert: Bool?

                beforeEach {
                    repository = MockFailurService()
                    model = PetListViewModel(respository: repository)
                    model.loadPets(callback: { displayAlert = true })
                }

                it("shows alert message") {
                    expect(displayAlert) == true
                }
            }
        }
    }
    
}

final class MockService: PetDetailsListRepository {

    func getDetailsList() -> Single<[Pet]> {
        return .just([.init(name: "tom", type: "cat", gender: "Male"), .init(name: "tina", type: "cat", gender: "Female")])
    }
}

final class MockFailurService: PetDetailsListRepository {

    func getDetailsList() -> Single<[Pet]> {
        return Single.error(RepositoryError.timedOut)
    }
}

@testable import PetShop
import Nimble
import Quick
import RxSwift
