// Copyright © 2018 MATHEW, Charlie. All rights reserved.

final class PersonClientSpec: QuickSpec {

    override func spec() {

        describe("a PersonClient") {

            var client: PersonClientType!
            var disposable: Disposable?

            beforeEach {
                client = PersonClient(client: MockHTTPClient())
            }

            afterEach {
                client = nil
                disposable?.dispose()
                disposable = nil
            }

            it("initialises") {
                expect(client).toNot(beNil())
            }

            describe("retrieving of APIOwnerResponse") {

                context("when successful") {

                    beforeEach {
                        client = PersonClient(client: MockHTTPClient())
                    }

                    it("retrieves the APIOwnerResponse") {
                        var response: [APIOwnerResponse]?

                        disposable = client
                            .getPetOwners()
                            .subscribe(onSuccess: { response = $0 })

                        expect(response).toEventuallyNot(beNil())
                    }
                }

                context("when unsuccessful") {

                    beforeEach {
                        client = PersonClient(client: MockHTTPErrorClient())
                    }

                    it("returns an error") {
                        var returnedError: Error?
                        disposable = client
                            .getPetOwners()
                            .subscribe(onError: { returnedError = $0 })

                        expect(returnedError).toEventuallyNot(beNil())
                        expect(returnedError).toEventually(beAnInstanceOf(APIError.self))
                    }
                }
            }

        }
    }
}

private final class MockHTTPClient: HTTPClientType {

    func singleDataTask<T>(with urlPath: String) -> PrimitiveSequence<SingleTrait, T> where T : Decodable {
        return Single.create { single in
            let response: [APIOwnerResponse] = try! [APIOwnerResponse]
                .decode(fromJSONData: Bundle(for: type(of: self))
                    .jsonData(forResource: "MockPetResponse"))
            single(.success(response as! T))
            return Disposables.create()
        }
    }
}

private final class MockHTTPErrorClient: HTTPClientType {

    func singleDataTask<T>(with urlPath: String) -> PrimitiveSequence<SingleTrait, T> where T : Decodable {
        return .error(APIError.dataNotFound)
    }
}

@testable import PetShop
import Nimble
import Quick
import RxSwift
