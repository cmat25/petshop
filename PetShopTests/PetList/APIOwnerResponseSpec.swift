// Copyright © 2018 MATHEW, Charlie. All rights reserved.

final class APIOwnerResponseSpec: QuickSpec {

    override func spec() {

        describe("a APIOwnerResponse") {

            var response: [APIOwnerResponse]!

            afterEach {
                response = nil
            }

            describe("decoding") {

                beforeEach {
                    let json: Data = Bundle(for: type(of: self)).jsonData(forResource: "MockPetResponse")
                    response = try? [APIOwnerResponse].decode(fromJSONData: json)
                }

                afterEach {
                    response = nil
                }

                it("decodes response from a JSON file") {
                    expect(response).toNot(beNil())
                    expect(response).to(haveCount(1))
                }

                it("decodes the person") {
                    expect(response.first?.name) == "Bob"
                    expect(response.first?.gender) == "Male"
                    expect(response.first?.age) == 23
                    expect(response.first?.pets).to(haveCount(2))
                }

                it("decodes the person's pets") {
                    expect(response.first?.pets?.first?.name) == "Garfield"
                    expect(response.first?.pets?.first?.type) == "Cat"
                    expect(response.first?.pets?.last?.name) == "Fido"
                    expect(response.first?.pets?.last?.type) == "Dog"
                }
            }
        }
    }
}

@testable import PetShop
import Nimble
import Quick
import RxSwift
