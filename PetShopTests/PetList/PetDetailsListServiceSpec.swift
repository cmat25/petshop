// Copyright © 2018 MATHEW, Charlie. All rights reserved.

final class PetDetailsListServiceSpec: QuickSpec {

    override func spec() {

        describe("a PetDetailsListService") {

            var service: PetDetailsListRepository!

            beforeEach {
                service = PetDetailsListService(client: MockPersonClientSuccess(), mapper: MockAPIPetMapper())
            }

            afterEach {
                service = nil
            }

            it("initialises") {
                expect(service).toNot(beNil())
            }

            describe("getting the product list") {

                var disposable: Disposable!
                var output: [Pet]!
                var error: Error!

                afterEach {
                    disposable?.dispose()
                    disposable = nil
                    output = nil
                    error = nil
                }

                context("when request succeeds") {

                    beforeEach {
                        service = PetDetailsListService(client: MockPersonClientSuccess(), mapper: MockAPIPetMapper())
                    }

                    afterEach {
                        service = nil
                    }

                    it("returns a mapped domain model") {
                        disposable = service
                            .getDetailsList()
                            .subscribe(onSuccess: { output = $0 })
                        expect(output).toEventuallyNot(beNil())
                    }
                }

                context("when the request fail") {

                    beforeEach {
                        service = PetDetailsListService(client: MockPersonClientFailure(), mapper: MockAPIPetMapper())
                    }

                    afterEach {
                        service = nil
                    }

                    it("returns an error") {
                        disposable = service
                            .getDetailsList()
                            .subscribe(
                                onSuccess: { output = $0 },
                                onError: { error = $0 }
                        )

                        expect(output).toEventually(beNil())
                        expect(error).toEventuallyNot(beNil())
                        expect(error).toEventually(beAnInstanceOf(RepositoryError.self))
                    }
                }
            }
        }
    }
}

private final class MockPersonClientSuccess: PersonClientType {

    func getPetOwners() -> Single<[APIOwnerResponse]> {
        return .just([])
    }
}

private final class MockPersonClientFailure: PersonClientType {

    func getPetOwners() -> Single<[APIOwnerResponse]> {
        return Single.error(APIError.dataNotFound)
    }
}

private final class MockAPIPetMapper: APIPetMapping {

    func mapToPets(from ownerResponse: [APIOwnerResponse]) -> [Pet] {
        return [Pet.init(name: nil, type: nil, gender: nil)]
    }
}

@testable import PetShop
import Nimble
import Quick
import RxSwift
