// Copyright © 2018 MATHEW, Charlie. All rights reserved.

final class APIPetMapperSpec: QuickSpec {

    override func spec() {

        describe("a APIPetMapper") {

            var mapper: APIPetMapper!

            beforeEach {
                mapper = APIPetMapper()
            }

            afterEach {
                mapper = nil
            }

            it("initialises") {
                expect(mapper).toNot(beNil())
            }

            describe("mapping of pets from the API") {

                var pets: [Pet]!

                afterEach {
                    pets = nil
                }

                context("when there is valid data") {

                    beforeEach {
                        pets = mapper.mapToPets(from: MockOwners.withData)
                    }

                    it("maps to array of pet for given owners") {
                        expect(pets).notTo(beNil())
                        expect(pets).to(haveCount(2))
                    }

                    it("maps to pet model") {
                        expect(pets.first?.name) == "name"
                        expect(pets.first?.type) == "dog"
                        expect(pets.last?.name) == "name"
                        expect(pets.last?.type) == "cat"
                    }
                }

                context("when there is no valid data") {

                    beforeEach {
                        pets = mapper.mapToPets(from: MockOwners.withoutData)
                    }

                    it("maps to domain layer model") {
                        expect(pets).notTo(beNil())
                    }

                    it("maps to no timetable object") {
                        expect(pets).to(haveCount(0))
                    }
                }
            }
        }
    }

}

private struct MockOwners {

    static var withData: [APIOwnerResponse] {
        let pets: [APIOwnerResponse.Pet] = [.init(name: "name", type: "dog"), .init(name: "name", type: "cat")]
        let owners: [APIOwnerResponse] = [APIOwnerResponse.init(name: nil, gender: "male", age: 13, pets: pets)]
        return owners
    }

    static var withoutData: [APIOwnerResponse]  {
        return []
    }
}

@testable import PetShop
import Nimble
import Quick
import RxSwift
